Модуль **query**
================

.. automodule:: visualization.query

.. autoclass:: visualization.query.QueryBuilder
   :members:

.. autofunction:: visualization.query.import_code_from_folder

.. autofunction:: visualization.query.import_code_from_string

.. autofunction:: visualization.query.dot_cfg_for_func

.. autofunction:: visualization.query.close_current_project

.. autofunction:: visualization.query.non_external_func_names
