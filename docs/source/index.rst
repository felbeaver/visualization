.. Visualization documentation master file, created by
   sphinx-quickstart on Fri Feb 19 20:39:29 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Добро пожаловать в документацию пакета Visualization!
=====================================================

Visualization - это Python пакет для построения различных визуализаций исходного кода на языке C. Данный пакет может строить следующие визуализации:

* **CFG** (Control Flow Graph) - граф потока управления 
* **DDG** (Data Dependency Graph) - граф зависимости по данным
* тримаппинг
* полиметрические представления
* эволюционные радары (только для модулей)

Пакет использует инструмент Joern для построения визуализаций. Данный 
пакет по-сути является оберткой над клиентом библиотеки 
:mod:`cpgqls_client`.

Использование
-------------

Для использования пакета необходимо установить инструмент Joern. Быстрая 
установка Joern с использование заранее собранных исходников::

   wget https://github.com/ShiftLeftSecurity/joern/releases/latest/download/joern-install.sh
   chmod +x ./joern-install.sh
   sudo ./joern-install.sh
   joern

   Compiling (synthetic)/ammonite/predef/interpBridge.sc
   Compiling (synthetic)/ammonite/predef/replBridge.sc
   Compiling (synthetic)/ammonite/predef/DefaultPredef.sc
   Compiling /home/tmp/shiftleft/joern/(console)
   
        ██╗ ██████╗ ███████╗██████╗ ███╗   ██╗
        ██║██╔═══██╗██╔════╝██╔══██╗████╗  ██║
        ██║██║   ██║█████╗  ██████╔╝██╔██╗ ██║
   ██   ██║██║   ██║██╔══╝  ██╔══██╗██║╚██╗██║
   ╚█████╔╝╚██████╔╝███████╗██║  ██║██║ ╚████║
    ╚════╝  ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝
   
   joern>

Подробная документация по установке инструмента доступна по адресу 
https://docs.joern.io/installation

.. toctree::
   :maxdepth: 2
   :caption: Содержимое:

   graphs
   query
   result

Указатели и таблицы
===================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
