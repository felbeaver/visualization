Модуль **graphs**
=================

.. automodule:: visualization.graphs

.. autofunction:: visualization.graphs.cfg_for_folder

.. autofunction:: visualization.graphs.cfg_for_file

