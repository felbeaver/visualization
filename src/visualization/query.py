"""Модуль **query** содержит вспомогательные функции для формирования 
строк с запросами к серверу Joern.

Для большинства запросов в модуле есть функции, которые формируют запрос.
Класс :class:`QueryBuilder` должен использоваться только для формирования 
специфичных запросов (для которых нет соответствующей функции).
"""
from enum import Enum

class QueryBuilder:
    """Класс **QueryBuilder** позволяет формировать строку с запросом.

    Запрос формируется за счет добавления фраз запроса. Для каждой фразы 
    можно указать ее аргументы при помощи метода :meth:`add_phrase`. 
    Класс запоминает каждую добавленную фразу, запомненные фразы можно 
    сбросить при помощи метода :meth:`reset`.
    """
    def __init__(self):
        """Конструктор класса, не принимает никаких параметров."""
        self._args = {}
    
    def build(self):
        """Возвращает строку запроса с добавленными фразами.

        :returns: строка запроса
        :rtype: string
        """
        return '.'.join(['%s(%s)' % (k, ', '.join(['"' + v + '"' for v in vs])) 
            if vs else k for k, vs in self._args.items()])

    def add_phrase(self, phrase, *args):
        """Добавляет фразу в запрос. Фразой может быть, например, 
        importCode или project, то есть названия, которые указываются 
        через точку в командах Joern.
        
        Данный метод принимает переменное количество параметров, в 
        *args* передаются аргументы фразы.

        :param phrase: фраза запроса
        :type phrase: string
        :param args: аргументы фразы (каждый аргумент имеет тип *string*)
        """
        self._args[phrase] = args

    def reset(self):
        """Сбрасывает запомненные фразы."""
        self._args.clear()


def import_code_from_folder(path):
    """Формирует запрос для импортирования исходного кода из директории 
    с проектом.

    :param string path: абсолютный путь к проекту
    :returns: строка с запросом
    :rtype: string
    """
    builder = QueryBuilder()
    builder.add_phrase('importCode', path)
    return builder.build()


def import_code_from_string(code):
    """Формирует запрос для импортирования исходного кода, который 
    храниться в строке *code*.

    :param string code: строка с исходным кодом
    :returns: строка с запросом
    :rtype: string
    """
    builder = QueryBuilder()
    builder.add_phrase('importCode')
    builder.add_phrase('c')
    builder.add_phrase('fromString', code)
    return builder.build()


def dot_cfg_for_func(func_name):
    """Формирует запрос для получения DOT представления графа потока 
    управления (CFG) функции с именем *func_name*.

    :param string func_name: имя функции
    :returns: строка с запросом
    :rtype: string
    """
    builder = QueryBuilder()
    builder.add_phrase('cpg')
    builder.add_phrase('method', func_name)
    builder.add_phrase('dotCfg')
    builder.add_phrase('l')
    return builder.build()


def close_current_project():
    """Формирует запрос для закрытия текущего проекта Joern.

    :returns: строка с запросом
    :rtype: string
    """
    return 'close(project.name)'

def delete_current_project():
    """Формирует запрос для закрытия и удаления текущего проекта Joern.

    :returns: строка с запросом
    :rtype: string
    """
    return 'delete(project.name)'


def non_external_func_names():
    """Формирует запрос для получения имен функций, которые принадлежат 
    текущему проекту (не внешние импортированные функции).

    :returns: строка с запросом
    :rtype: string
    """
    return 'cpg.method.filter(_.isExternal == false).name.l'

