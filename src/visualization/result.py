"""Модуль **result** содерит функции для преобразования результатов 
запросов к серверу Joern в формат удобный для работы в Python.

Модуль преобразовывает различные представления данных Joern в типы данных 
Python.
"""
import re
import ast

def as_list(result):
    """Преобразовывает результат запроса в список Python *list*.

    :param result: результат запроса
    :type result: JSON объект
    :returns: результат запроса в виде списка
    :rtype: list
    """
    pattern = re.compile(r'List\(\n?((.+\n?)+)\)', re.MULTILINE)
    match = pattern.search(result['stdout'])
    return ast.literal_eval('[' + match.group(1) + ']')

