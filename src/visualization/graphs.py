"""Модуль **graphs** содержит функции для построения визуализаций 
исходного кода на языке C в виде графов.

Функции модуля используют класс :class:`cpgqls_client.CPGQLSClient`, 
который предоставляет интерфейс клиента для взаимодействия с сервером 
Joern. Данный класс позволяет выполнять запросы к серверу и получать 
результат выполнения этих запросов.
"""
from visualization import query as q
from visualization import result as r

def cfg_for_folder(client, path, funcs=None):
    """Возвращает граф потока управления (CFG) для функций проекта, 
    который находиться в директории *path*.

    Возвращает граф потока управления (CFG) в DOT формате для функций, 
    названия который указываются в параметре *funcs*. Если *funcs* = 
    None (по-умолчанию), то графы строятся для каждой функции проекта.

    :param client: клиент для взаимодействия с сервером Joern
    :type client: :class:`cpgqls_client.CPGQLSClient`
    :param string path: абсолютный путь к проекту
    :param list funcs: список названий функций
    :returns: словарь вида (название_функции:dot_представление_графа) (*string*:*string*)
    :rtype: dict
    """
    client.execute(q.import_code_from_folder(path))
    if not funcs:
        result = client.execute(q.non_external_func_names())
        funcs = r.as_list(result)

    dots = {}

    for f in funcs:
        result = client.execute(q.dot_cfg_for_func(f))
        dots[f] = r.as_list(result)[0]

    client.execute(q.delete_current_project())
    return dots


def cfg_for_code(client, code, funcs=None):
    """Возвращает граф потока управления (CFG) для функций исхдоного 
    кода, который храниться в памяти. Предпологается, что это строка 
    исходного кода одного файла.

    Возвращает граф потока управления (CFG) в DOT формате для функций, 
    названия который указываются в параметре *funcs*. Если *funcs* = 
    None (по-умолчанию), то графы строятся для каждой функции файла.

    :param client: клиент для взаимодействия с сервером Joern
    :type client: :class:`cpgqls_client.CPGQLSClient`
    :param string code: строка с исходным кодом
    :param list funcs: список названий функций
    :returns: словарь вида (название_функции:dot_представление_графа) (*string*:*string*)
    :rtype: dict
    """
    code = repr(code).strip("'").replace('"', '\\"')

    client.execute(q.import_code_from_string(code))
    if not funcs:
        result = client.execute(q.non_external_func_names())
        funcs = r.as_list(result)

    dots = {}

    for f in funcs:
        result = client.execute(q.dot_cfg_for_func(f))
        dots[f] = r.as_list(result)[0]

    client.execute(q.close_current_project())
    return dots


def cfg_for_file(client, path, funcs=None):
    """Возвращает граф потока управления (CFG) для функций файла, 
    который находиться по пути *path*.

    Возвращает граф потока управления (CFG) в DOT формате для функций, 
    названия который указываются в параметре *funcs*. Если *funcs* = 
    None (по-умолчанию), то графы строятся для каждой функции файла.

    :param client: клиент для взаимодействия с сервером Joern
    :type client: :class:`cpgqls_client.CPGQLSClient`
    :param string path: абсолютный путь к файлу
    :param list funcs: список названий функций
    :returns: словарь вида (название_функции:dot_представление_графа) (*string*:*string*)
    :rtype: dict
    """
    with open(path, 'r') as f:
        code = f.read()

    return cfg_for_code(client, code, funcs)

