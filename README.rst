Visualization
=============

Visualization - это Python пакет для построения различных визуализаций исходного кода на языке C. Данный пакет может строить следующие визуализации:

* **CFG** (Control Flow Graph) - граф потока управления 
* **DDG** (Data Dependency Graph) - граф зависимости по данным
* тримаппинг
* полиметрические представления
* эволюционные радары (только для модулей)

Использование
-------------

Для использования пакета необходимо установить инструмент Joern. Быстрая 
установка Joern с использование заранее собранных исходников::

   wget https://github.com/ShiftLeftSecurity/joern/releases/latest/download/joern-install.sh
   chmod +x ./joern-install.sh
   sudo ./joern-install.sh
   joern

   Compiling (synthetic)/ammonite/predef/interpBridge.sc
   Compiling (synthetic)/ammonite/predef/replBridge.sc
   Compiling (synthetic)/ammonite/predef/DefaultPredef.sc
   Compiling /home/tmp/shiftleft/joern/(console)
   
        ██╗ ██████╗ ███████╗██████╗ ███╗   ██╗
        ██║██╔═══██╗██╔════╝██╔══██╗████╗  ██║
        ██║██║   ██║█████╗  ██████╔╝██╔██╗ ██║
   ██   ██║██║   ██║██╔══╝  ██╔══██╗██║╚██╗██║
   ╚█████╔╝╚██████╔╝███████╗██║  ██║██║ ╚████║
    ╚════╝  ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝
   
   joern>

Подробная документация по установке инструмента доступна по адресу 
https://docs.joern.io/installation

Пакет Visualization может быть использовать из командной строки или программно. Ниже представлен пример кода с использованием API пакета::

   import visualization

   visualization.foo('source_file.c') # returns a value

Пример использования из командной строки::

   $ visualization cfg source_file.c -o graph.json

Подробная документации пакета находиться на ReadTheDocs: https://visualization.readthedocs.io/.

Вклад в разработку
------------------

Pull запросы всегда приветствуются. Для главных (больших) изменений, пожалуйста, откройте проблему (issue) для обсуждения того, что вы хотели бы изменить.

Перед изменениями убедитесь в том, что тесты обновлены подходящим образом.

Лицензия
--------

`MIT <https://choosealicense.com/licenses/mit/>`_

